function g2 = calculateg2()

global cube

g2 = 0;

if(cube.front.N.Piece ~= 2)
g2 = g2+4;
end
if(cube.front.S.Piece ~= 8)
g2 = g2+4;
end
if(cube.front.E.Piece ~= 6)
g2 = g2+4;
end
if(cube.front.W.Piece ~= 4)
g2 = g2+4;
end

if(cube.top.N.Piece ~= 19)
g2 = g2+4;
end
if(cube.top.E.Piece ~= 12)
g2 = g2+4;
end
if(cube.top.W.Piece ~= 10)
g2 = g2+4;
end

if(cube.back.E.Piece ~= 23)
g2 = g2+4;
end
if(cube.back.W.Piece ~= 21)
g2 = g2+4;
end
if(cube.back.S.Piece ~= 25)
g2 = g2+4;
end

if(cube.bottom.W.Piece ~= 13)
g2 = g2+4;
end
if(cube.bottom.E.Piece ~= 15)
g2 = g2+4;
end


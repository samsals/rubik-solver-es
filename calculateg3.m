function g3 = calculateg3()

global cube;

g3 = 0;

if(cube.front.NW.Piece ~= 1)
    g3 = g3+6;
end
if(cube.front.NE.Piece ~= 3)
    g3 = g3+6;
end
if(cube.front.SW.Piece ~= 7)
    g3 = g3+6;
end
if(cube.front.SE.Piece ~= 9)
    g3 = g3+6;
end

if(cube.back.NW.Piece ~= 18)
    g3 = g3+6;
end
if(cube.back.NE.Piece ~= 20)
    g3 = g3+6;
end
if(cube.back.SW.Piece ~= 24)
    g3 = g3+6;
end
if(cube.back.SE.Piece ~= 26)
    g3 = g3+6;
end

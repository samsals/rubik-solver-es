p = generatePopulation();

for t = 1:20
    f = zeros(size(p,1),1);
    for i = 1:size(p,1)
        global cube;
        cube = generateCube();
        iface_apply(p{i});
        f(i) = iface_calculateFitness();
    end
    [sorted_x, index] = sort(f,'ascend');
    index = index(1:20);
    newgen = p(index);
    min(f)
    g = newgen;
    for i = 1:2:size(newgen,1)
        temp1 = newgen{i,:};
        temp2 = newgen{i+1,:};

        temp = temp1(1:25);
        temp1(1:25) = temp2(1:25);
        temp2(1:25) = temp;

        temp = temp1(51:75);
        temp1(51:75) = temp2(51:75);
        temp2(51:75) = temp;

        temp = temp1(101:125);
        temp1(101:125) = temp2(101:125);
        temp2(101:125) = temp;

        temp = temp1(151:175);
        temp1(151:175) = temp2(151:175);
        temp2(151:175) = temp;

        temp = temp1(201:225);
        temp1(201:225) = temp2(201:225);
        temp2(201:225) = temp;

        g{i,:} = temp1;
        g{i+1,:} = temp2;
    end
    p = [g; newgen];
    ix = randperm(size(p,1));
    p = p(ix);
end
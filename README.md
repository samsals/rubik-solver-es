# Matlab Rubik Cube Solver #
![Build Status](https://img.shields.io/teamcity/codebetter/bt428.svg)
![Release](https://img.shields.io/github/release/qubyte/rubidium.svg)
![License](https://img.shields.io/badge/license-LGPL-blue.svg)

A Rubik cube solver using Evolutionary strategy Algorithm.
For educational Purposes.
### Have Any inqueries? ###

*Contact ssalama.mo@gmail.com
function drawCube(cube)


colorRef=[1,0,0;
       1,.5,0;
       0,0,1;
       0,.8,.3;
       1,1,0;
       1,1,1;];

   %figure
   hold on

for a=1:6
    switch a
        case 1 %front
            x0=0;
            y0=0;
        case 2 %BAck
            x0 = 600;
            y0=0;
        case 3 %top
            x0=0;
            y0=300;
        case 4 %bottom
            x0=0;
            y0=-300;
        case 5 %left
            x0=-300;
            y0=0;
        case 6 %right
            x0=300;
            y0=0;
    end
    for j=1:9
        switch mod(j,3)
            case 1 %NW, W, SW
                x1=-100;
            case 2 %N, C, S
                x1=0;
            case 0 %NE, E, SE
                x1=100;
        end
        switch floor((j-1)/3)
            case 0 %NW, N, NE
                y1=100;
            case 1 %W, C, E
                y1=0;
            case 2 %SW, S, SE
                y1=-100;
        end
        eval(['colorInd=cube.' cube.faceLst{a} '.' cube.tileLst{j} '.Color;']);
        eval(['pieceInd=cube.' cube.faceLst{a} '.' cube.tileLst{j} '.Piece;']);
        rectangle('Position',[x0+x1-50,y0+y1-50,100,100],'FaceColor',colorRef(colorInd,:))
        text(x0+x1,y0+y1,num2str(pieceInd));
    end
end

plot([150,450],[-450,-150],'k--')
plot([-150,-450],[-450,-150],'k--')
plot([150,450],[450,150],'k--')
plot([-150,-450],[450,150],'k--')
hold off

end


function [ cube ] = generateCube()

cube.faceLst={'front','back','top','bottom','left','right'};
cube.tileLst={'NW','N','NE','W','C','E','SW','S','SE'};

for i=1:6
    for j=1:9
        eval(['cube.' cube.faceLst{i} '.' cube.tileLst{j} '.Color= i;']);
        eval(['cube.' cube.faceLst{i} '.' cube.tileLst{j} '.Piece= ind2piece(i,j);']);
    end
end


end



function solution=twes(cube,m,l)
    %this function implement the thistlewaite algorithm
    %cube represents the unsolved cube.
    %m is number of individuals in population. according to paper it should
    %50,000!
    %l is minimum number of fitted cubes which fires phase transition operation
    
    %generate population
    for i=1:m
        population(i).cube=cube;
        population(i).fit=9999999;
        population(i).operations='';
    end
    solvedCubes=phaseSolve(population,l,0);
    for j=1:length(m)
        population(j)=solvedCubes(randi(length(solvedCubes)));
    end
    solvedCubes=phaseSolve(population,l,1);
    for j=1:length(m)
        population(j)=solvedCubes(randi(length(solvedCubes)));
    end
    solvedCubes=phaseSolve(population,l,2);
    for j=1:length(m)
        population(j)=solvedCubes(randi(length(solvedCubes)));
    end
    solution=phaseSolve(population,l,3);
    return
end

function solvedCubes=phaseSolve(population,l,phaseNum)
    maxGenerations=150;
    generationsNum=0;
    semiSolvedCubes=struct('cube',{},'fit',[],'operations',[]);
    while generationsNum<maxGenerations
        %mutate every individual then calculate fittness.
        for k=1:length(population)
            population(k)=mutateCube(population(k),phaseNum);
            population(k).fit=calculateFitness(population(k).cube,phaseNum,length(population(k).operations));
        end
        %check for solved cubes,if so end this phase.
        population = nestedSortStruct(population, 'fit');
        for i=1:l
            if population(i).fit == length(population(i).operations)
                semiSolvedCubes(end+1)=population(i);            
            end
        end
        
        if (length(semiSolvedCubes)==l)
            solvedCubes=semiSolvedCubes;
            return;
        elseif (length(semiSolvedCubes)<l)
            for p=1:l
                semiSolvedCubes(end+1)=population(p);
            end
            for j=1:length(population)
                population(j)=semiSolvedCubes(randi(length(semiSolvedCubes)));
            end
            semiSolvedCubes=struct('cube',{},'fit',[],'operations',[]);
        end        
    generationsNum=1+generationsNum;
    generationsNum
    end
    solvedCubes=0;
end
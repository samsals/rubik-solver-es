function c=iface_moveRightUp(cube)


cube = rotateCube(cube,'Z',-1);
cube=moveCube(cube,1,-1);
cube = rotateCube(cube,'Z',1);
c=cube;
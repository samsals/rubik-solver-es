function mutatedCube = mutateCube(cubey, phase)
if phase==0
    moves={{'F'},{'R'},{'U'},{'B'},{'L'},{'D'}};
    sequence='';
    randomInt=randi(7);
    for i=1:randomInt
        sequence=strcat(sequence,char(moves{randi(6)}));
    end
    cubey.cube=iface_apply(sequence,cubey.cube);
    cubey.operations=strcat(cubey.operations,sequence);
    mutatedCube=cubey;
elseif phase==1
    moves=[{'F'}, {'U'}, {'B'}, {'D'}, {'R2'}, {'L2'}];
    sequence='';
    randomInt=randi(13);
    for i=1:randomInt
        sequence=strcat(sequence,char(moves{randi(6)}));
    end
    cubey.cube=iface_apply(sequence,cubey.cube);
    cubey.operations=strcat(cubey.operations,sequence);
    mutatedCube=cubey;
elseif phase==2
    moves=[{'U'}, {'D'}, {'R2'}, {'L2'}, {'F2'}, {'B2'}];
    sequence='';
    randomInt=randi(15);
    for i=1:randomInt
        sequence=strcat(sequence,char(moves{randi(6)}));
    end
    cubey.cube=iface_apply(sequence,cubey.cube);
    cubey.operations=strcat(cubey.operations,sequence);
    mutatedCube=cubey;
elseif phase==3
    moves=[{'F2'}, {'R2'}, {'U2'}, {'B2'}, {'L2'}, {'D2'}];
    sequence='';
    randomInt=randi(17);
    for i=1:randomInt
        sequence=strcat(sequence,char(moves{randi(6)}));
    end
    cubey.cube=iface_apply(sequence,cubey.cube);
    cubey.operations=strcat(cubey.operations,sequence);
    mutatedCube=cubey;
end
%main
close all
global cube

cube=generateCube();
figure
drawCube(cube)

iface_turnDown();
figure
drawCube(cube)

iface_moveBottomLeft();
figure
drawCube(cube)

iface_moveLeftUp();
figure
drawCube(cube)
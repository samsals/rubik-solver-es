function c=iface_moveMidDown()


cube = rotateCube(cube,'Z',-1);
cube=moveCube(cube,2,1);
cube = rotateCube(cube,'Z',1);
c=cube;
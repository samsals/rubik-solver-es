function p=ind2piece(i,j)

switch i
        case 1 %Front face: 9 unique pieces
           p=j; %1-9
        case 2 %Back face: 9 unique pieces
           p=17+j; %18-26
        case 3 %Top face: 3 unique pieces and 6 repeats
            if j<=3 %Back pieces
                p=21-j; %18-20
            elseif j>=7 %Front pieces
                p=j-6; %1-3
            else %Unique pieces
                p=9-3+j; %10-12
            end
        case 4 %Bottom face: 3 unique pieces and 6 repeats
            if j<=3 %Front pieces
                p=6+j; %7-9
            elseif j>=7 %Back pieces
                p=33-j; %24-26
            else %Unique pieces
                p=9+j; %13-15
            end
        case 5 %Left face: 1 unique piece, 8 repeats
            switch j
                case 1
                    p=20; %Back 3:
                case 2
                    p=10; %Top 4
                case 3
                    p=1; %Front 1
                case 4
                    p=23; %Back 6
                case 5
                    p=16; %Unique
                case 6
                    p=4; %Front 4
                case 7
                    p=26; %Back 9
                case 8
                    p=13; %bottom 4
                case 9
                    p=7; %Front 7
            end
        case 6 %Right face: 1 unique,  repeats
            switch j
                case 1
                    p=3; %Front 3
                case 2
                    p=12; %Top 6
                case 3
                    p=18; %Back 1
                case 4
                    p=6; %Front 6
                case 5
                    p=17; %Unique
                case 6
                    p=21; %Back 4
                case 7
                    p=9; %Front 9
                case 8
                    p=15; %bottom 6
                case 9
                    p=24; %back 7
            end

    end
end

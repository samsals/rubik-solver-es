function c=iface_moveLeftUp(cube)


cube = rotateCube(cube,'Z',-1);
cube=moveCube(cube,3,-1);
cube = rotateCube(cube,'Z',1);
c=cube;
function g = iface_calculateFitness()
%By Ahmed ElSallamy%
%Calculate the fitness of the current cube%

g1 = calculateg1();
g2 = calculateg2();
g3 = calculateg3();

g = g1+g2+g3;

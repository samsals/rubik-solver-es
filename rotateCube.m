function newCube = rotateCube(cube,axis,dir)
%Function that does not change the actual cube, just the perspective of it

newCube=cube;

%Check that dir is an integer
if mod(dir,1)~=0
    disp('Error: dir is not an integer size, not performing rotation')
    return
end

%Check that axis is 'X', 'Y' or 'Z'
if (axis~='X')&&(axis~='Y')&&(axis~='Z')
    disp('Error: direction is not ''X'', ''Y'', or ''Z'', not changing anything')
    return
end

%Check that dir is 1 or -1
dir=mod(dir,4); %All rotations are expressed as compositions of CCW rotations
if dir==0
    disp('Warning: requested rotation of null size')
    return
elseif dir>1
    cubeAux=cube;
    for i=1:dir
        cubeAux=rotateCube(cubeAux,axis,1);
    end
    newCube=cubeAux;
    return
else %dir=1
switch axis
    case 'X'
            %if AXIS ='X', DIR='1': TOP -> front -> BOTTOM -> BACK (tile change for back);
            %RIGHT rotates counterclockwise in a compass, LEFT does clockwise
            newCube.front = cube.top;
            newCube.bottom = cube.front;
            newCube.back = rotateFace(cube.bottom,2);
            newCube.top = rotateFace(cube.back,2);
            newCube.right = rotateFace(cube.right,1);
            newCube.left = rotateFace(cube.left,-1);
        
    case 'Y'
            %if AXIS = 'Y', DIR =1: left-> front -> right -> back; top rotates ccw,
            %bottom is cw
            newCube.front = cube.left;
            newCube.right = cube.front;
            newCube.back = cube.right;
            newCube.left = cube.back;
            newCube.top = rotateFace(cube.top,1);
            newCube.bottom = rotateFace(cube.bottom,-1);

    case 'Z' %Can be achieved as a composition: 'X','Y',-'X'
        newCube=rotateCube(cube,'X',1);
        newCube=rotateCube(newCube,'Y',1);
        newCube=rotateCube(newCube,'X',-1);
end









end


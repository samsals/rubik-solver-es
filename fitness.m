function f = fitness(p)
    
    f = zeros(size(p,1),1);
    for i = 1:size(p,1)
        global cube;
        cube = generateCube();
        iface_apply(p{i});
        f(i) = iface_calculateFitness();
    end
    [sorted_x, index] = sort(f,'ascend');
    index = index(1:20);
    newgen = p(index);
    
    g = newgen;

for i = 1:2:size(newgen,1)
    temp1 = newgen(i,:);
    temp2 = newgen(i+1,:);
    
    temp = temp1(1:50);
    temp1(1:50) = temp2(1:50);
    temp2(1:50) = temp;
    
    temp = temp1(51:100);
    temp1(51:100) = temp2(51:100);
    temp2(51:100) = temp;

    temp = temp1(101:150);
    temp1(101:150) = temp2(101:150);
    temp2(101:150) = temp;
    
    temp = temp1(151:200);
    temp1(151:200) = temp2(151:200);
    temp2(151:200) = temp;
    
    temp = temp1(201:250);
    temp1(201:250) = temp2(201:250);
    temp2(201:250) = temp;
    
    g{i,:} = temp1;
    g{i+1,:} = temp2;
end

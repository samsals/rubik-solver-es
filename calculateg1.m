function g1 = calculateg1()

global cube;
g1 = 0;


if(cube.front.C.Color~=cube.front.N.Color)
    g1 = g1+1;
end
if(cube.front.C.Color~=cube.front.NW.Color)
    g1 = g1+1;
end
if(cube.front.C.Color~=cube.front.NE.Color)
    g1 = g1+1;
end
if(cube.front.C.Color~=cube.front.E.Color)
    g1 = g1+1;
end
if(cube.front.C.Color~=cube.front.SE.Color)
    g1 = g1+1;
end
if(cube.front.C.Color~=cube.front.S.Color)
    g1 = g1+1;
end
if(cube.front.C.Color~=cube.front.W.Color)
    g1 = g1+1;
end
if(cube.front.C.Color~=cube.front.SW.Color)
    g1 = g1+1;
end

if(cube.back.C.Color~=cube.back.N.Color)
    g1 = g1+1;
end
if(cube.back.C.Color~=cube.back.NE.Color)
    g1 = g1+1;
end
if(cube.back.C.Color~=cube.back.NW.Color)
    g1 = g1+1;
end
if(cube.back.C.Color~=cube.back.E.Color)
    g1 = g1+1;
end
if(cube.back.C.Color~=cube.back.SE.Color)
    g1 = g1+1;
end
if(cube.back.C.Color~=cube.back.SW.Color)
    g1 = g1+1;
end
if(cube.back.C.Color~=cube.back.S.Color)
    g1 = g1+1;
end
if(cube.back.C.Color~=cube.back.W.Color)
    g1 = g1+1;
end

if(cube.top.C.Color~=cube.top.N.Color)
    g1 = g1+1;
end
if(cube.top.C.Color~=cube.top.NE.Color)
    g1 = g1+1;
end
if(cube.top.C.Color~=cube.top.NW.Color)
    g1 = g1+1;
end
if(cube.top.C.Color~=cube.top.E.Color)
    g1 = g1+1;
end
if(cube.top.C.Color~=cube.top.W.Color)
    g1 = g1+1;
end
if(cube.top.C.Color~=cube.top.S.Color)
    g1 = g1+1;
end
if(cube.top.C.Color~=cube.top.SW.Color)
    g1 = g1+1;
end
if(cube.top.C.Color~=cube.top.SE.Color)
    g1 = g1+1;
end

if(cube.bottom.C.Color~=cube.bottom.N.Color)
    g1 = g1+1;
end
if(cube.bottom.C.Color~=cube.bottom.NE.Color)
    g1 = g1+1;
end
if(cube.bottom.C.Color~=cube.bottom.NW.Color)
    g1 = g1+1;
end
if(cube.bottom.C.Color~=cube.bottom.S.Color)
    g1 = g1+1;
end
if(cube.bottom.C.Color~=cube.bottom.W.Color)
    g1 = g1+1;
end
if(cube.bottom.C.Color~=cube.bottom.E.Color)
    g1 = g1+1;
end
if(cube.bottom.C.Color~=cube.bottom.SE.Color)
    g1 = g1+1;
end
if(cube.bottom.C.Color~=cube.bottom.SW.Color)
    g1 = g1+1;
end

if(cube.left.C.Color~=cube.left.SW.Color)
    g1 = g1+1;
end
if(cube.left.C.Color~=cube.left.S.Color)
    g1 = g1+1;
end
if(cube.left.C.Color~=cube.left.W.Color)
    g1 = g1+1;
end
if(cube.left.C.Color~=cube.left.SE.Color)
    g1 = g1+1;
end
if(cube.left.C.Color~=cube.left.E.Color)
    g1 = g1+1;
end
if(cube.left.C.Color~=cube.left.NW.Color)
    g1 = g1+1;
end
if(cube.left.C.Color~=cube.left.NE.Color)
    g1 = g1+1;
end
if(cube.left.C.Color~=cube.left.N.Color)
    g1 = g1+1;
end

if(cube.right.C.Color~=cube.right.N.Color)
    g1 = g1+1;
end
if(cube.right.C.Color~=cube.right.NE.Color)
    g1 = g1+1;
end
if(cube.right.C.Color~=cube.right.W.Color)
    g1 = g1+1;
end
if(cube.right.C.Color~=cube.right.NW.Color)
    g1 = g1+1;
end
if(cube.right.C.Color~=cube.right.E.Color)
    g1 = g1+1;
end
if(cube.right.C.Color~=cube.right.SE.Color)
    g1 = g1+1;
end
if(cube.right.C.Color~=cube.right.SW.Color)
    g1 = g1+1;
end
if(cube.right.C.Color~=cube.right.S.Color)
    g1 = g1+1;
end

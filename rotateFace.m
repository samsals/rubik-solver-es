function [newFace] = rotateFace(face, dir)

%dir =1 means CCW
%dir =-1 means CW

copyFace=face;
dir=mod(dir,4); %Change all rotations to a composition of CCW rotations

if dir==1
   newFace.NW = copyFace.NE;
   newFace.N = copyFace.E;
   newFace.NE = copyFace.SE;
   newFace.E = copyFace.S;
   newFace.SE = copyFace.SW;
   newFace.S = copyFace.W;
   newFace.SW = copyFace.NW;
   newFace.W = copyFace.N;
   newFace.C = copyFace.C;
elseif dir>1
    for i=1:dir
        copyFace=rotateFace(copyFace,1);
    end
    newFace=copyFace;
end


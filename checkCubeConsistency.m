function [consistent] = checkCubeConsistency(cube)
%This function verifies that a cube is physically consistent: meaning that
%all expected pieces exist as they should:
%1) number of faces of each color is 9
%2) one center piece of each color
%3) the middle sides are consistent: there are no middle sides with colors
%that belong in opposing surfaces
%4) the corners are consistent: there are no corners with opposing colors,
%and there are no corners with two tiles of the same color.

faceLst={'front','back','top','bottom','left','right'};
tileLst={'C','N','W','S','E','NW','NE','SE','SW'};
for i=1:6 %Check each face
    for j=1:9
        %Get color of each piece
        eval(['T(i,j) = cube.' faceLst{i} '.' tileLst{j} ';']);
    end
end

%Check 9 tiles of each color
aux=sort(T(:));
consistent0 = (~any(aux(1:9)~=1))&&(~any(aux(10:18)~=2))&&(~any(aux(19:27)~=3))&&(~any(aux(28:36)~=4))&&(~any(aux(37:45)~=5))&&(~any(aux(46:54)~=6));

%Check 1 center tile of each color
aux=sort(T(:,1));
consistent1 = (aux(1)==1)&&(~any(diff(aux)~=1));

%Check 4 side tiles of each color
aux=T(2:5,:);
aux=sort(aux(:));
consistent2 = (~any(aux(1:4)~=1))&&(~any(aux(5:8)~=2))&&(~any(aux(9:12)~=3))&&(~any(aux(13:16)~=4))&&(~any(aux(17:20)~=5))&&(~any(aux(21:24)~=6));

%Check 4 corner tiles of each color
aux=T(6:9,:);
aux=sort(aux(:));
consistent3 = (~any(aux(1:4)~=1))&&(~any(aux(5:8)~=2))&&(~any(aux(9:12)~=3))&&(~any(aux(13:16)~=4))&&(~any(aux(17:20)~=5))&&(~any(aux(21:24)~=6));

%TO DO:
%Check relative position of side tiles: 1 tile that is 1 =(1,3), 2 =(1,4), 3 =(1,5),
%4 =(1,6), 1 that is 5= (2,3), 6 = (2,4), 7= (2,5), 8=(2,6), 1 that is 9 =(3,5) 10=(3,6), 1 that
%is 11=(4,5), 12=(4,6)

end


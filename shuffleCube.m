function [cube] = shuffleCube(cube)

for i=1:200 %Do 20 moves
    %Select random rotation to front
    lst='XYZ';
    axis=1+floor(2.9999*rand(1)); %dim
    dir=sign(randn(1)); %dir
    %Perform
    cube=rotateCube(cube,lst(axis),dir);
    %Select random move: row,dir
    row=1+floor(2.9999*rand(1)); %dim
    dir=sign(randn(1)); %dir
    %Perform
    cube=moveCube(cube,row,dir);
end


end


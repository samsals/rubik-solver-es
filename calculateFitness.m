function f = calculateFitness(cube, phase, c)
    if(phase==0)
        w = 0;
        if(cube.top.N.Color==5 || cube.top.N.Color==6)
            w = w+1;
        elseif((cube.top.N.Color==1 || cube.top.N.Color==2)&&(cube.back.N.Color== 3 || cube.back.N.Color==4))
            w = w+1;
        end
        if(cube.top.S.Color==5 || cube.top.S.Color==6)
            w = w+1;
        elseif((cube.top.S.Color==1 || cube.top.S.Color==2)&&(cube.front.N.Color==3 || cube.front.N.Color==4))
            w = w+1;
        end
        if(cube.top.E.Color==5 || cube.top.E.Color==6)
            w = w+1;
        elseif((cube.top.E.Color==1 || cube.top.E.Color==2)&&(cube.right.N.Color==3 || cube.right.N.Color==4))
            w = w+1;
        end
        if(cube.top.W.Color==5 || cube.top.W.Color==6)
            w = w+1;
        elseif((cube.top.W.Color==1 || cube.top.W.Color==2)&&(cube.left.N.Color==3 || cube.left.N.Color==4))
            w = w+1;
        end
        
        if(cube.bottom.N.Color==5 || cube.bottom.N.Color==6)
            w = w+1;
        elseif((cube.bottom.N.Color==1 || cube.bottom.N.Color==2)&&(cube.front.S.Color== 3 || cube.front.S.Color==4))
            w = w+1;
        end
        if(cube.bottom.S.Color==5 || cube.bottom.S.Color==6)
            w = w+1;
        elseif((cube.bottom.S.Color==1 || cube.bottom.S.Color==2)&&(cube.back.S.Color==3 || cube.back.S.Color==4))
            w = w+1;
        end
        if(cube.bottom.E.Color==5 || cube.bottom.E.Color==6)
            w = w+1;
        elseif((cube.bottom.E.Color==1 || cube.bottom.E.Color==2)&&(cube.right.S.Color==3 || cube.right.S.Color==4))
            w = w+1;
        end
        if(cube.bottom.W.Color==5 || cube.bottom.W.Color==6)
            w = w+1;
        elseif((cube.bottom.W.Color==1 || cube.bottom.W.Color==2)&&(cube.left.S.Color==3 || cube.left.S.Color==4))
            w = w+1;
        end
        
        if(cube.front.E.Color==5 || cube.front.E.Color==6)
            w = w+1;
        elseif((cube.front.E.Color==1 || cube.front.E.Color==2)&&(cube.right.W.Color==3 || cube.right.W.Color==4))
            w = w+1;
        end
        if(cube.front.W.Color==5 || cube.front.W.Color==6)
            w = w+1;
        elseif((cube.front.W.Color==1 || cube.front.W.Color==2)&&(cube.left.E.Color==3 || cube.left.E.Color==4))
            w = w+1;
        end
        if(cube.back.E.Color==5 || cube.back.E.Color==6)
            w = w+1;
        elseif((cube.back.E.Color==1 || cube.back.E.Color==2)&&(cube.left.W.Color==3 || cube.left.W.Color==4))
            w = w+1;
        end
        if(cube.back.W.Color==5 || cube.back.W.Color==6)
            w = w+1;
        elseif((cube.back.W.Color==1 || cube.back.W.Color==2)&&(cube.right.E.Color==3 || cube.right.E.Color==4))
            w = w+1;
        end
        f = 5*(2*w) + c;
    elseif (phase==1)
        w = 0;
        if(cube.front.E.Piece~=4 && cube.front.E.Piece~=6 && cube.front.E.Piece~=21 && cube.front.E.Piece~=23)
            w = w+1;
        end            
        if(cube.front.W.Piece~=4 && cube.front.W.Piece~=6 && cube.front.W.Piece~=21 && cube.front.W.Piece~=23)
            w = w+1;
        end
        if(cube.back.E.Piece~=4 && cube.back.E.Piece~=6 && cube.back.E.Piece~=21 && cube.back.E.Piece~=23)
            w = w+1;
        end
        if(cube.back.W.Piece~=4 && cube.back.W.Piece~=6 && cube.back.W.Piece~=21 && cube.back.W.Piece~=23)
            w = w+1;
        end
        f = 5*(2*w) + c;
    elseif (phase==2)
        v = 0;
        if(cube.top.NE.Piece~=1 && cube.top.NE.Piece~=3 && cube.top.NE.Piece~=18 && cube.top.NE.Piece~=20)
            v = v+1;
        end
        if(cube.top.NW.Piece~=1 && cube.top.NW.Piece~=3 && cube.top.NW.Piece~=18 && cube.top.NW.Piece~=20)
            v = v+1;
        end
        if(cube.top.SE.Piece~=1 && cube.top.SE.Piece~=3 && cube.top.SE.Piece~=20 && cube.top.SE.Piece~=18)
            v = v+1;
        end
        if(cube.top.SW.Piece~=1 && cube.top.SW.Piece~=3 && cube.top.SW.Piece~=20 && cube.top.SW.Piece~=18)
            v = v+1;
        end
        if(cube.bottom.SW.Piece~=7 && cube.bottom.SW.Piece~=9 && cube.bottom.SW.Piece~=24 && cube.bottom.SW.Piece~=26)
            v = v+1;
        end
        if(cube.bottom.SE.Piece~=7 && cube.bottom.SE.Piece~=9 && cube.bottom.SE.Piece~=24 && cube.bottom.SE.Piece~=26)
            v = v+1;
        end
        if(cube.bottom.NW.Piece~=7 && cube.bottom.NW.Piece~=9 && cube.bottom.NW.Piece~=24 && cube.bottom.NW.Piece~=26)
            v = v+1;
        end
        if(cube.bottom.NE.Piece~=7 && cube.bottom.NE.Piece~=9 && cube.bottom.NE.Piece~=24 && cube.bottom.NE.Piece~=26)
            v = v+1;
        end
        
        f = 10*(4*v) + c;
    elseif (phase==3)
        x = 0;
        if(cube.bottom.N.Piece~=8 && cube.bottom.N.Piece~=13 && cube.bottom.N.Piece~=15 && cube.bottom.N.Piece~=25)
            x = x+1;
        end
        if(cube.bottom.S.Piece~=8 && cube.bottom.S.Piece~=13 && cube.bottom.S.Piece~=15 && cube.bottom.S.Piece~=25)
            x = x+1;
        end
        if(cube.bottom.E.Piece~=8 && cube.bottom.E.Piece~=13 && cube.bottom.E.Piece~=15 && cube.bottom.E.Piece~=25)
            x = x+1;
        end
        if(cube.bottom.W.Piece~=8 && cube.bottom.W.Piece~=13 && cube.bottom.W.Piece~=15 && cube.bottom.W.Piece~=25)
            x = x+1;
        end
        
        if(cube.top.N.Piece~=2 && cube.top.N.Piece~=10 && cube.top.N.Piece~=12 && cube.top.N.Piece~=19)
            x = x+1;
        end
        if(cube.top.S.Piece~=2 && cube.top.S.Piece~=10 && cube.top.S.Piece~=12 && cube.top.S.Piece~=19)
            x = x+1;
        end
        if(cube.top.E.Piece~=2 && cube.top.E.Piece~=10 && cube.top.E.Piece~=12 && cube.top.E.Piece~=19)
            x = x+1;
        end
        if(cube.top.W.Piece~=2 && cube.top.W.Piece~=10 && cube.top.W.Piece~=12 && cube.top.W.Piece~=19)
            x = x+1;
        end
    end
end
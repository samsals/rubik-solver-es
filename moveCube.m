 function newCube=moveCube(cube,row,dir)
 %function that performs actual movements in the cube
 %movements are given in reference to the currently frontal face of the
 %cube, and only shifting of rows (horizontally) are allowed
 
 switch dir
     case 1 %front to right      
        newCube=cube;
         switch row
             case 1 %top most = only NE, N, NW tiles are affected in left, front, right, back, full face is rotated for top
                 newCube.top=rotateFace(cube.top,1); %Top face rotation
                 lst={'NW','N','NE'};
                 for i=1:3
                    eval(['newCube.right.' lst{i} '=cube.front.' lst{i} ';']);
                    eval(['newCube.front.' lst{i} '=cube.left.' lst{i} ';']);
                    eval(['newCube.left.' lst{i} '=cube.back.' lst{i} ';']);
                    eval(['newCube.back.' lst{i} '=cube.right.' lst{i} ';']);
                 end
                 
             case 2 %middle
                 lst={'W','C','E'};
                 for i=1:3
                    eval(['newCube.right.' lst{i} '=cube.front.' lst{i} ';']);
                    eval(['newCube.front.' lst{i} '=cube.left.' lst{i} ';']);
                    eval(['newCube.left.' lst{i} '=cube.back.' lst{i} ';']);
                    eval(['newCube.back.' lst{i} '=cube.right.' lst{i} ';']);
                 end
                 
             case 3 %bottom most
                 newCube.bottom=rotateFace(cube.bottom,-1); %Bottom face rotation
                 lst={'SW','S','SE'};
                 for i=1:3
                    eval(['newCube.right.' lst{i} '=cube.front.' lst{i} ';']);
                    eval(['newCube.front.' lst{i} '=cube.left.' lst{i} ';']);
                    eval(['newCube.left.' lst{i} '=cube.back.' lst{i} ';']);
                    eval(['newCube.back.' lst{i} '=cube.right.' lst{i} ';']);
                 end
                 
         end
     case -1 %front to left = implemented as 3 succesive front to right
         cubeAux=cube;
         for i=1:3
            cubeAux=moveCube(cubeAux,row,1);
         end
         newCube=cubeAux;
 end

end


function surfCube(cube)

colorRef=[1,0,0;
       1,.5,0;
       0,0,1;
       0,.8,.3;
       1,1,0;
       1,1,1;];

h=gcf;
hold on
for a=1:6
    switch a
        case 1 %front
            C=[0,0,150];
            vE=[100,0,0];
            vN=[0,100,0];
        case 2 %BAck
            C=[0,0,-150];
            vE=[-100,0,0];
            vN=[0,100,0];
        case 3 %top
            C=[0,150,0];
            vN=[0,0,-100];
            vE=[100,0,0];
        case 4 %bottom
            C=[0,-150,0];
            vE=[100,0,0];
            vN=[0,0,100];
        case 5 %left
            C=[-150,0,0];
            vE=[0,0,100];
            vN=[0,100,0];
        case 6 %right
            C=[150,0,0];
            vE=[0,0,-100];
            vN=[0,100,0];
    end
    for j=1:9
        %Get 4 corners of each tile
        switch mod(j,3)
            case 1 %NW, W, SW
                v1=-vE;
            case 2 %N, C, S
                v1=0;
            case 0 %NE, E, SE
                v1=vE;
        end
        switch floor((j-1)/3)
            case 0 %NW, N, NE
                v2=vN;
            case 1 %W, C, E
                v2=0;
            case 2 %SW, S, SE
                v2=-vN;
        end
        j;
        vC = C + v1 + v2; %Center of tile
        %Plot
        eval(['colorInd=cube.' cube.faceLst{a} '.' cube.tileLst{j} '.Color;']);
        surf(vC(1) + .5*[-(vN(1)+vE(1)) (vN(1) - vE(1));  -(vN(1)-vE(1)) (vN(1) + vE(1))],vC(2) + .5*[-(vN(2)+vE(2)) (vN(2) - vE(2));  -(vN(2)-vE(2)) (vN(2) + vE(2))],vC(3) + .5*[-(vN(3)+vE(3)) (vN(3) - vE(3));  -(vN(3)-vE(3)) (vN(3) + vE(3))],colorInd)
    end
end
set(h,'ColorMap',colorRef)
axis equal
hold off
end


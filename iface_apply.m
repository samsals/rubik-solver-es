function c=iface_apply(s,cube)
%By Ahmed ElSallamy%
%Parsing string of moves and apply it to the cube%
for i = 1:length(s)
    cw = true;
    if((i~=length(s))&&(s(i+1)=='i'))
        cw = false;
    end
    if(s(i)=='i')
    elseif(s(i)=='2')
        s(i) = s(i-1);
    end
    if(s(i)=='F')
        cube=iface_turnUp(cube);
        if(cw)
            cube=iface_moveTopLeft(cube);
        else cube=iface_moveTopRight(cube);
        end
        cube=iface_turnDown(cube);
    elseif(s(i)=='R')
        if(cw)
            cube=iface_moveRightUp(cube);
        else
            cube=iface_moveRightUp(cube);
            cube=iface_moveRightUp(cube);
            cube=iface_moveRightUp(cube);
        end
    elseif(s(i)=='U')
        if(cw)
            cube=iface_moveTopLeft(cube);
        else
            cube=iface_moveTopRight(cube);
        end
    elseif(s(i)=='B')
        cube=iface_turnDown(cube);
        if(cw)
            cube=iface_moveTopRight(cube);
        else
            cube=iface_moveTopLeft(cube);
        end
        cube=iface_turnUp(cube);
    elseif(s(i)=='L')
        if(cw)
            cube=iface_moveLeftUp(cube);
        else
            cube=iface_moveLeftDown(cube);
        end
    elseif(s(i)=='D')
        if(cw)
            cube=iface_moveBottomRight(cube);
        else
            cube=iface_moveBottomLeft(cube);
        end
    end

end
c=cube;